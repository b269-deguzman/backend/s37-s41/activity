// s37 Activity
const mongoose = require ('mongoose');

const courseSchema = mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
		// required: [true, "isActive value is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
		// required: [true, "price is required"]
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "userId is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = new mongoose.model("Course", courseSchema);
