const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		// Requires the data for this our fields/properties to be included when creating a record.
		// "true" value defines if the field is required or not
		// "First name is required" value is the message that will be printed out in our terminal when data is not present
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	// The "enrollments" property/field will be an array of objects containing the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course
	enrollments : [
		{
			courseId : {
				type : String,
				required : [true, "Course ID is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Enrolled"
			}
		}
	]
});

// "User" = collection
// Models use Schemas and they act as the middleman from the server (JS code) to our database
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
module.exports = new mongoose.model("User", userSchema);