const Course = require("../models/Course");
const auth = require('../auth');

// Create a new course
// module.exports.addCourse = (reqBody)=>{
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});
// 	if(auth.verify){
// 			return newCourse.save().then((course,error)=>{
// 				// Course creation failed
// 				if(error){
// 				return false;
// 				// Course creation successful
// 				}else{
// 				return true;
// 				}
// 			})
		
// 	}
	
// }

module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	}; 
	// User is not an admin
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value)=>{
		return {value};
	});
};

// Retrieve ALL Courses
module.exports.getAllCourses = ()=>{
	return Course.find({}).then(result =>{
		return result;
	})
}

// Retrieve ACTIVE courses
module.exports.getAllActive = ()=>{
	return Course.find({isActive: true}).then(result =>{
		return result;
	});
};

// Retrieve specific course 
module.exports.getCourse = (reqParams)=>{
	return Course.findById(reqParams.courseId).then(result =>{
		return result;
	});
};

// Update a course
module.exports.updateCourse = (reqParams,reqBody)=>{
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

// Archive a course
// module.exports.archiveCourse= (reqParams, data)=>{
// 	if(data.isAdmin){
// 		return Course.findByIdAndRemove(reqParams.courseId, data.course).then((course,error)=>{
// 			if(error){
// 				return false;
// 			}else{
// 				return true;
// 			}
// 		})
// 	}
// 	let message = Promise.resolve('User must be ADMIN to access this!');
// 	return message.then((value)=>{
// 		return {value};
// 	});	
// }
module.exports.archiveCourse= (reqParams)=>{
	let updatedActiveField= {
		isActive: false
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updatedActiveField).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
}

